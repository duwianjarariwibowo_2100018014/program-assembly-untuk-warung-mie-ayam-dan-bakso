.MODEL SMALL
.CODE
ORG 100H
start:
jmp mulai
nama db 13,10,'Nama Anda	: $'
lanjut	db 13,10,'Tekan "y" untuk melihat menu berikutnya'  
db 13,10,'atau "Enter" untuk Langsung Memesan   >>>>>>>>>>$' 
menu_habis db 13,10,'Menu sudah habis ketik "y" untuk Memesan $' 
tampung_nama	db 30,?,30 dup(?)
daftar	db 13,10,'+-----------------------------------------------------------+'
	db 13,10,'|                      DAFTAR MENU                          |'           
	db 13,10,'+-----------------------------------------------------------+' 
	db 13,10,'|                     Paket 1 Orang                         |'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|No |Makan           |Minum                  |Harga    |kode|'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|01 |1.Mie Ayam      |1.Es Teh  /Teh Hangat  |Rp.15.000|  a |'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|02 |1.Mie Ayam      |1.Es Jeruk/Jeruk Hangat|Rp.15.000|  b |'
    db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|03 |1.Bakso         |1.Es Teh  /Teh Hangat  |Rp.15.000|  c |'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|04 |1.Bakso         |1.Es Jeruk/Jeruk Hangat|Rp.15.000|  d |'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|05 |1.Mie Ayam+Bakso|1.Es Teh  /Teh Hangat  |Rp.18.000|  e |'
	db 13,10,'+---+----------------+-----------------------+---------+----|' 
	db 13,10,'|06 |1.Mie Ayam+Bakso|1.Es Jeruk/Jeruk Hangat|Rp.18.000|  f |'
	db 13,10,'+---+----------------+-----------------------+---------+----|''$'
daftar2	db 13,10,'+-----------------------------------------------------------+'
	db 13,10,'|                      DAFTAR MENU                          |'           
	db 13,10,'+-----------------------------------------------------------+' 
	db 13,10,'|                     Paket 2 Orang                         |'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|No |Makan           |Minum                  |Harga    |kode|'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|07 |2.Mie Ayam      |2.Es Teh  /Teh Hangat  |Rp.30.000|  g |'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|08 |2.Mie Ayam      |2.Es Jeruk/Jeruk Hangat|Rp.30.000|  h |'
    db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|09 |2.Bakso         |2.Es Teh  /Teh Hangat  |Rp.30.000|  i |'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|10 |2.Bakso         |2.Es Jeruk/Jeruk Hangat|Rp.30.000|  j |'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|11 |2.Mie Ayam+Bakso|2.Es Teh  /Teh Hangat  |Rp.35.000|  k |'
	db 13,10,'+---+----------------+-----------------------+---------+----|' 
	db 13,10,'|12 |2.Mie Ayam+Bakso|2.Es Jeruk/Jeruk Hangat|Rp.35.000|  l |'
	db 13,10,'+---+----------------+-----------------------+---------+----|''$'
daftar3	db 13,10,'+-----------------------------------------------------------+'
	db 13,10,'|                      DAFTAR MENU                          |'           
	db 13,10,'+-----------------------------------------------------------+' 
	db 13,10,'|                     Paket 3 Orang                         |'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|No |Makan           |Minum                  |Harga    |kode|'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|13 |3.Mie Ayam      |3.Es Teh  /Teh Hangat  |Rp.45.000|  m |'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|14 |3.Mie Ayam      |3.Es Jeruk/Jeruk Hangat|Rp.45.000|  n |'
    db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|15 |3.Bakso         |3.Es Teh  /Teh Hangat  |Rp.45.000|  o |'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|16 |3.Bakso         |3.Es Jeruk/Jeruk Hangat|Rp.45.000|  p |'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|17 |3.Mie Ayam+Bakso|3.Es Teh  /Teh Hangat  |Rp.50.000|  q |'
	db 13,10,'+---+----------------+-----------------------+---------+----|' 
	db 13,10,'|18 |3.Mie Ayam+Bakso|3.Es Jeruk/Jeruk Hangat|Rp.50.000|  r |'
	db 13,10,'+---+----------------+-----------------------+---------+----|''$'
daftar4	db 13,10,'+-----------------------------------------------------------+'
	db 13,10,'|                      DAFTAR MENU                          |'           
	db 13,10,'+-----------------------------------------------------------+' 
	db 13,10,'|                     Paket 4 Orang                         |'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|No |Makan           |Minum                  |Harga    |kode|'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|19 |4.Mie Ayam      |4.Es Teh  /Teh Hangat  |Rp.60.000|  s |'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|20 |4.Mie Ayam      |4.Es Jeruk/Jeruk Hangat|Rp.60.000|  t |'
    db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|21 |4.Bakso         |4.Es Teh  /Teh Hangat  |Rp.60.000|  u |'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|22 |4.Bakso         |4.Es Jeruk/Jeruk Hangat|Rp.60.000|  v |'
	db 13,10,'+---+----------------+-----------------------+---------+----|'
	db 13,10,'|23 |4.Mie Ayam+Bakso|4.Es Teh  /Teh Hangat  |Rp.65.000|  w |'
	db 13,10,'+---+----------------+-----------------------+---------+----|' 
	db 13,10,'|24 |4.Mie Ayam+Bakso|4.Es Jeruk/Jeruk Hangat|Rp.65.000|  x |'
	db 13,10,'+---+----------------+-----------------------+---------+----|''$'
error db 13,10,'KODE YANG ANDA MASUKKAN SALAH $'
pilih_mtr db 13,10,'Silahkan masukan kode :$'
mulai:
	mov ah,09h
	lea dx,nama
	int 21h
	mov ah,0ah
	lea dx,tampung_nama
	int 21h
	mov ah,09h
	mov dx,offset daftar
	int 21h
	mov ah,09h
	mov dx,offset lanjut
	int 21h
	mov ah,01h
	int 21h
	cmp al ,'y'
	je page2
	jne proses
page2:
	mov ah,09h
	mov dx,offset daftar2
	int 21h
	mov ah,09h
	mov dx,offset lanjut
	int 21h
	mov ah,01h
	int 21h
	cmp al,'y' 
	je page3
	jne proses
page3:
	mov ah,09h
	mov dx,offset daftar3
	int 21h
	mov ah,09h
	mov dx,offset lanjut
	int 21h
	mov ah,01h
	int 21h
	cmp al,'y'
	je page4
	jne proses
page4:
	mov ah,09h
	mov dx,offset daftar4
	int 21h
	mov ah,09h
    mov dx,offset menu_habis
	int 21h
	mov ah,01h
	int 21h
	cmp al,'y'
    mov ah,09h
	jmp proses
	jne error_msg
error_msg:
	mov ah,09h
	mov dx,offset error
	int 21h
	int 20h
proses:
	mov ah,09h
	mov dx,offset pilih_mtr
	int 21h
	mov ah,1
	int 21h
	mov bl,al
	
	cmp bl,'a'
	je hasil1
	
	cmp bl,'b'
	je hasil2

	cmp bl,'c'
	je hasil3

	cmp bl,'d'
	je hasil4

	cmp bl,'e'
	je hasil5

	cmp bl,'f'
	je hasil6

	cmp bl,'g'
	je hasil7

	cmp bl,'h'
	je hasil8
	
	cmp bl,'i'
	je hasil9
	
	cmp bl,'j'
	je hasil10  
	
	cmp bl,'k'
	je hasil11 
	
	cmp bl,'l'
	je hasil12

	cmp bl,'m'
	je hasil13

	cmp bl,'n'
	je hasil14

	cmp bl,'o'
	je hasil15

	cmp bl,'p'
	je hasil16

	cmp bl,'q'
	je hasil17

	cmp bl,'r'
	je hasil18

	cmp bl,'s'
	je hasil19

	cmp bl,'t'
	je hasil20

	cmp bl,'u'
	je hasil21

	cmp bl,'v'
	je hasil22

	cmp bl,'w'
	je hasil23

	cmp bl,'x'
	je hasil24
	



jne error_msg
;-----------------------------------------------------------
hasil1:
	mov ah,09h
	lea dx,teks1  
	int 21h
	int 20h

hasil2:
	mov ah,09h
	lea dx,teks2
	int 21h
	int 20h

hasil3:
	mov ah,09h
	lea dx,teks3
	int 21h
	int 20h

hasil4:
	mov ah,09h
	lea dx,teks4
	int 21h
	int 20h

hasil5:
	mov ah,09h
	lea dx,teks5
	int 21h
	int 20h

hasil6:
	mov ah,09h
	lea dx,teks6
	int 21h
	int 20h

hasil7:
	mov ah,09h
	lea dx,teks7
	int 21h
	int 20h

hasil8:
	mov ah,09h
	lea dx,teks8
	int 21h
	int 20h
hasil9:
	mov ah,09h
	lea dx,teks9
	int 21h
	int 20h

hasil10:
	mov ah,09h
	lea dx,teks10
	int 21h
	int 20h

hasil11:
	mov ah,09h
	lea dx,teks11
	int 21h
	int 20h
	
hasil12:
	mov ah,09h
	lea dx,teks12
	int 21h
	int 20h
	
hasil13:
	mov ah,09h
	lea dx,teks13
	int 21h
	int 20h
	
hasil14:
	mov ah,09h
	lea dx,teks14
	int 21h
	int 20h
	
hasil15:
	mov ah,09h
	lea dx,teks15
	int 21h
	int 20h
	
hasil16:
	mov ah,09h
	lea dx,teks16
	int 21h
	int 20h
	
hasil17:
	mov ah,09h
	lea dx,teks17
	int 21h
	int 20h
	
hasil18:
	mov ah,09h
	lea dx,teks18
	int 21h
	int 20h
	
hasil19:
	mov ah,09h
	lea dx,teks19
	int 21h
	int 20h
hasil20:
	mov ah,09h
	lea dx,teks20
	int 21h
	int 20h
	
hasil21:
	mov ah,09h
	lea dx,teks21
	int 21h
	int 20h
	
hasil22:
	mov ah,09h
	lea dx,teks22
	int 21h
	int 20h
	
hasil23:
	mov ah,09h
	lea dx,teks23
	int 21h
	int 20h
	
hasil24:
	mov ah,09h
	lea dx,teks24
	int 21h
	int 20h
;------------------------------------------------------------------
teks1	db 13,10,'Anda memilih paket 1 orang'
	db 13,10,'1.Mie Ayam'
	db 13,10,'1.Es teh / Teh Hangat'
	db 13,10,'total harga Rp.15.000'
	db 13,10,'terimakasih $'  
teks2	db 13,10,'Anda memilih paket 1 orang'
	db 13,10,'1.Mie Ayam' 
	db 13,10,'1.Es Jeruk / Jeruk Hangat'
	db 13,10,'total harga Rp.15.000'
	db 13,10,'terimakasih $'
teks3	db 13,10,'Anda memilih paket 1 orang'
	db 13,10,'1.Bakso'
	db 13,10,'1.Es teh / Teh Hangat'
	db 13,10,'total harga Rp.15.000'
	db 13,10,'terimakasih $'
teks4	db 13,10,'Anda memilih paket 1 orang'
	db 13,10,'1.Bakso'
	db 13,10,'1.Es Jeruk / Jeruk Hangat'
	db 13,10,'total hargaRp.15.000'
	db 13,10,'terimakasih $'
teks5	db 13,10,'Anda memilih paket 1 orang'
	db 13,10,'1.Mie Ayam+Bakso'
	db 13,10,'1.Es teh / Teh Hangat'
	db 13,10,'total harga Rp.18.000'
	db 13,10,'terimakasih $'
teks6	db 13,10,'Anda memilih paket 1 orang'
	db 13,10,'1.Mie Ayam+Bakso'
	db 13,10,'1.Es Jeruk / Jeruk Hangat'
	db 13,10,'total harga Rp.18.000'
	db 13,10,'terimakasih $'
teks7	db 13,10,'Anda memilih paket 2 orang'
	db 13,10,'2.Mie Ayam'
	db 13,10,'2.Es teh / Teh Hangat'
	db 13,10,'total harga Rp.30.000'
	db 13,10,'terimakasih $'
teks8	db 13,10,'Anda memilih paket 2 orang'
	db 13,10,'2.Mie Ayam' 
	db 13,10,'2.Es Jeruk / Jeruk Hangat'
	db 13,10,'total harga Rp.30.000'
	db 13,10,'terimakasih $'
teks9	db 13,10,'Anda memilih paket 2 orang'
	db 13,10,'2.Bakso' 
	db 13,10,'2.Es teh / Teh Hangat'
	db 13,10,'total harga Rp.30.000'
	db 13,10,'terimakasih $'
teks10	db 13,10,'Anda memilih paket 2 orang'
	db 13,10,'2.Bakso'
	db 13,10,'2.Es Jeruk / Jeruk Hangat'
	db 13,10,'total harga Rp.30.000'
	db 13,10,'terimakasih $'
teks11	db 13,10,'Anda memilih paket 2 orang'
	db 13,10,'2.Mie Ayam+Bakso'
	db 13,10,'2.Es teh / Teh Hangat'
	db 13,10,'total harga Rp.35.000'
	db 13,10,'terimakasih $'
teks12	db 13,10,'Anda memilih paket 2 orang'
	db 13,10,'2.Mie Ayam+Bakso'
	db 13,10,'2.Es Jeruk / Jeruk Hangat'
	db 13,10,'total harga Rp.35.000'
	db 13,10,'terimakasih $'
teks13	db 13,10,'Anda memilih paket 3 orang'
	db 13,10,'3.Mie Ayam'   
	db 13,10,'3.Es teh / Teh Hangat'
	db 13,10,'total harga Rp.45.000'
	db 13,10,'terimakasih $'
teks14	db 13,10,'Anda memilih paket 3 orang'
	db 13,10,'3.Mie Ayam'
	db 13,10,'3.Es Jeruk / Jeruk Hangat'
	db 13,10,'total harga Rp.45.000'
	db 13,10,'terimakasih $'
teks15	db 13,10,'Anda memilih paket 3 orang'
	db 13,10,'3.Bakso'
	db 13,10,'3.Es teh / Teh Hangat'
	db 13,10,'total harga Rp.45.000'
	db 13,10,'terimakasih $'
teks16	db 13,10,'Anda memilih paket 3 orang'
	db 13,10,'3.Bakso'
	db 13,10,'3.Es Jeruk / Jeruk Hangat'
	db 13,10,'total harga Rp.45.000'
	db 13,10,'terimakasih $'
teks17	db 13,10,'Anda memilih paket 3 orang'
	db 13,10,'3.Mie Ayam+Bakso'
	db 13,10,'3.Es teh / Teh Hangat'
	db 13,10,'total harga Rp.50.000'
	db 13,10,'terimakasih $'
teks18	db 13,10,'Anda memilih paket 3 orang'
	db 13,10,'3.Mie Ayam+Bakso'
	db 13,10,'3.Es Jeruk / Jeruk Hangat'
	db 13,10,'total harga Rp.50.000'
	db 13,10,'terimakasih $'
teks19	db 13,10,'Anda memilih paket 4 orang'
	db 13,10,'4.Mie Ayam'
	db 13,10,'4.Es teh / Teh Hangat'
	db 13,10,'total harga Rp.60.000'
	db 13,10,'terimakasih $'
teks20	db 13,10,'Anda memilih paket 4 orang'
	db 13,10,'4.Mie Ayam'
	db 13,10,'4.Es Jeruk / Jeruk Hangat'
	db 13,10,'total harga Rp.60.000'
	db 13,10,'terimakasih $'
teks21	db 13,10,'Anda memilih paket 4 orang'
	db 13,10,'4.Bakso'
	db 13,10,'4.Es teh / Teh Hangat'
	db 13,10,'total harga Rp.60.000'
	db 13,10,'terimakasih $'
teks22	db 13,10,'Anda memilih paket 4 orang'
	db 13,10,'4.Bakso'
	db 13,10,'4.Es Jeruk / Jeruk Hangat'
	db 13,10,'total harga Rp.60.000'
	db 13,10,'terimakasih $'
teks23	db 13,10,'Anda memilih paket 4 orang'
	db 13,10,'4.Mie Ayam+Bakso'
	db 13,10,'4.Es teh / Teh Hangat'
	db 13,10,'total harga Rp.65.000'
	db 13,10,'terimakasih $'
teks24	db 13,10,'Anda memilih paket 4 orang'
	db 13,10,'4.Mie Ayam+Bakso'
	db 13,10,'4.Es Jeruk / Jeruk Hangat'
	db 13,10,'total harga Rp.65.000'
	db 13,10,'terimakasih $' 
    
end start